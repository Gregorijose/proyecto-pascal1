program calculadorafibonacci;
uses crt;

var 
opc:integer;
opcion:char;
r:char;
m,a,p:int64;
num,contador:integer;
tody:boolean;
todyo:boolean;

BEGIN
 repeat

textcolor(lightcyan);
writeln('');
writeln('');		
writeln(	'***********BIENVENIDO A LA CALCULADORA FIBONACCI***********');
writeln('');
writeln('************************ HECHO POR ************************');
writeln('');
Writeln('*********************** JOSE VALDEZ ***********************');
writeln('');
writeln('********************** MENU PRINCIPAL ***********************');
writeln('');
writeln('');
writeln('---------------------********************-------------------------');	
			
			writeln('-Seleccione alguna de las opciones:');
			writeln('');
			writeln('*1: informacion sobre la sucesion de Fibonacci*');
			writeln('');
			writeln('*2: Calculo de Posicion*');
			writeln('');
			writeln('*3: Impresion de la sucesion*');
			writeln('');
			writeln('*4: Salida*');					
			opcion:=readkey; 
		
			
/////////////////{case}////////////////////////////////////////////////
			
			clrscr;
			
			
			case opcion of

		//informacion de fibonacci//
///////////////////////inicio de case 1//////////////////////////////
			'1':
				
				begin
					
				writeln('------------------Informacion------------------'); 
				writeln('');
				writeln('----------****En que consiste?****----------');
				writeln('');
				writeln('Se trata de una secuencia infinita de numeros naturales; a partir del 0 y el 1, se van sumando');
				writeln('a pares, de manera que cada numero es igual a la suma de sus dos anteriores, de manera que:');
				writeln('');
				writeln('Ejemplo:');
				writeln('');
				writeln('0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, etc');
				writeln('');
				writeln('----------****Quien expuso la sucesion de Fibonacci?****----------');
				writeln('');
				writeln('fue el matematico Leonardo de Pisa, mas conocido como Fibonacci, y en su "Libro de calculo", Fibonacci promovio,');
				writeln('el nuevo sistema de numeros, demostrando lo sencillo que era en comparacion con los numeros romanos que se usaban en toda Europa.');
				writeln('');
				writeln('----------****En que a#o se expuso?****----------');
				writeln('');
				writeln('Fibonacci  escribio en 1202 ');
				writeln('');
				writeln('----------****Que aplicaciones tiene en la actualidad?****----------');
				writeln('');
				writeln('Tiene numerosas aplicaciones en ciencias de la computación, matematicas y teoria de juego. ');
				writeln(' Tambien aparece en configuraciones biologicas, como por ejemplo en las ramas de los arboles, en la');
				writeln('disposicion de las hojas en el tallo, en la flora de la alcachofa y en el arreglo de un cono.');
				writeln('');
				writeln('Para volver a menu oprima enter');
				
				readln(r);
				
				
				clrscr;
					
					
					end; 
				
//////////////////////////fin de case 1//////////////////////////////
/////////////////////////inicio de case 2////////////////////////////
			'2':
			
				begin 
				
				clrscr;
				writeln('   -escriba un valor   ');
				writeln();
				readln(opc);
				
				case opc of
				
					0 : writeln('forma parte de la sucesion en la posicion 1');
					1 : writeln('forma parte de la sucesion en la posicion 2');
					2 : writeln('forma parte de la sucesion en la posicion 4');
					3 : writeln('forma parte de la sucesion en la posicion 5');
					5 : writeln('forma parte de la sucesion en la posicion 6');
					8 : writeln('forma parte de la sucesion en la posicion 7');
					13: writeln('forma parte de la sucesion en la posicion 9');
					21: writeln('forma parte de la sucesion en la posicion 10');
					34: writeln('forma parte de la sucesion en la posicion 11');
					55: writeln('forma parte de la sucesion en la posicion 12');
					89: writeln('forma parte de la sucesion en la posicion 13');
					144: writeln('forma parte de la sucesion en la posicion 14');
					233: writeln('forma parte de la sucesion en la posicion 15');
					377: writeln('forma parte de la sucesion en la posicion 16');
					610: writeln('forma parte de la sucesion en la posicion 17');
					987: writeln('forma parte de la sucesion en la posicion 18');
					1597: writeln('forma parte de la sucesion en la posicion 19');
					12584: writeln('forma parte de la sucesion en la posicion 20');
			
				else writeln(' -Este numero no forma parte de la sucesion de fibonacci ');
			
				end;	
			
					begin 
				
				
					writeln();
					writeln(' -Para volver a menu oprima enter');
				
					readln(r);
				
					clrscr;
					
	
					end;
						end;
//////////////////////////fin de case 2////////////////////////////
/////////////////////////inicio de case 3//////////////////////////
				
			'3':
				
				begin
				
				clrscr;
				tody:=true;

				while tody do
								
				begin
				
				contador:=3;
				todyo:=false;
				clrscr;
				
					writeln('Calculo de Serie de Fibonacci.');
					writeln;
					write('Ingrese numero de terminos de la serie a calcular: ');
					readln(num);
					writeln;
					while num<1 do
					begin
					write('Ingrese numero mayor que 0…');
					readln(num);
					writeln;
				
				end;
				
					m:=0;
					a:=1;
					p:=m+a;
					
					writeln('Serie de Fibonacci con ',num,' terminos:');
					writeln;
					
					if num=1 then
					writeln(m);
					if num=2 then
					writeln(m,' ',a);
					if num=3 then
					writeln(m,' ',a,' ',p);
					if num>3 then
					while contador<num do
					
				begin
             
					if todyo=false then
               
					begin
					
                    write(m,' ',a,' ',p,' ');
                    todyo:=true;
                    
                  end;
                  
						m:=a;
						a:=p;
						p:=m+a;
						write(p,' ');
						contador:=contador+1;
				
				  end;

					writeln;
					writeln;
					writeln('Desea continuar? y/n ');
					readln(r);
					if r <> 'y' then
					
					tody:=false;
					
					clrscr;
					writeln('--------------*****************************************---------------');
					writeln('--------------GRACIAS POR USAR LA CALCULADORA FIBONACCI---------------');
					writeln('--------------*****************************************---------------');
				
				end;
					end;
//////////////////////////////////fin de case 3////////////////////////
////////////////////////////inicio de case 4///////////////////////////
					
			'4':
				begin
				
					writeln('--------------*****************************************---------------');
					writeln('--------------GRACIAS POR USAR LA CALCULADORA FIBONACCI---------------');
					writeln('--------------*****************************************---------------');
					
				end;
					end;
/////////////////////////////fin del case//////////////////////////////

////////////////////until/////////////////////////////////////////////

			until(opcion='4');
						
END.

/Hecho po: jose Gregorio Valdez lopez C.I 29.864.222/
